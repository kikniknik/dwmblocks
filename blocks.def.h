//Modify this file to change what commands output to your statusbar and recompile using the make command.
static const Block blocks[] = {
    /* Icon,  Command,                      Update Interval, Update Signal, */
    {"",      "sb-uptime",                  5,               4},
    {"🐏",    "sb-memory",                  5,               5},
    {"| ",    "sb-battery",                 30,              3},
    {"| ",    "sb-wifi",                    60,              6},
    {"| ",    "sb-volume --bat",            600,             14},
    {"",      "sb-volume",                  0,               1},
    {" ",     "sb-volume --mic",            0,               9},
    {"| ⌨",   "sb-kblayout",                0,               2},
    {"| 󰅒> ", "sb-inactivity-lock dpms",    0,               13},
    {"",      "sb-inactivity-lock lock",    0,               12},
    {"",      "sb-inactivity-lock suspend", 0,               11},
    {"| ",    "sb-weather",                 5400,            7},
    {"⌚",    "date '+%b %d (%a) %H:%M'",   30,              8},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 5;
